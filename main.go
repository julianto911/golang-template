package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	cfg "gitlab.com/julianto0911/system-configuration"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	ginprometheus "github.com/zsais/go-gin-prometheus"
	"go.uber.org/zap"
)

func main() {
	configFile := "config.yaml"

	if len(os.Args) > 1 {
		configFile = os.Args[1]
	}

	ctx := cfg.SystemContext{}
	if err := ctx.Initiate(configFile); err != nil {
		log.Fatalf("can't initialize config: %v", err)
	}

	defer func() {
		err := ctx.DB.Close()
		if err != nil {
			ctx.L.Error("db close error", zap.Error(err))
		}
		defer ctx.L.Sync()
	}()

	ctx.L.Debug("zap init", zap.Any("Parameters", ctx))
	ctx.L.Info("init done")

	r := gin.New()
	gin.SetMode(gin.ReleaseMode)

	r.Use(gin.Recovery())

	pprof.Register(r)

	p := ginprometheus.NewPrometheus("gin")

	p.ReqCntURLLabelMappingFn = func(c *gin.Context) string {
		url := c.Request.URL
		url.RawQuery = ""
		return url.String()
	}
	p.Use(r)

	r.Static("/assets", "./assets")

	r.GET("/", pageLanding(&ctx))

	ctx.L.Info("gin init done")

	srv := &http.Server{
		Addr:    ":" + ctx.App.Port,
		Handler: r,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			ctx.L.Fatal("can't run webserver", zap.Error(err))
		}
	}()
	ctx.L.Info("Server initiated at ", zap.String("port ", ctx.App.Port))

	// gracefully shutdown
	quit := make(chan os.Signal, 2)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	ctx.L.Info("Shutdown Server ...")

	cts, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(cts); err != nil {
		ctx.L.Error("can't shutdown server", zap.Error(err))
	}

	ctx.L.Info("Server exiting")
}
