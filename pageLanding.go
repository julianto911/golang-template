package main

import (
	"html/template"

	"github.com/gin-gonic/gin"
	cfg "gitlab.com/julianto0911/system-configuration"
	"go.uber.org/zap"
)

func pageLanding(ctx *cfg.SystemContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		filename := "html/lp.gohtml"
		t, err := template.ParseFiles(filename)
		if err != nil {
			ctx.L.Warn("Invalid page", zap.Error(err))
			c.Abort()
			return
		}
		if err := t.Execute(c.Writer, nil); err != nil {
			ctx.L.Warn("Fail process page", zap.Error(err))
		}

	}
}
